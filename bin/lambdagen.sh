#!/bin/bash

set -e

LAMBDA_NAME=$1
LAMBDA_DESCRIPTION=$2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Check cookiecutter is installed and if not then install it
brew list cookiecutter > /dev/null || brew install cookiecutter
# Check yq is installed an if not install it
brew list yq  > /dev/null || brew install yq

# Run cookiecutter to generate most of the files
cookiecutter --no-input --output-dir "${SCRIPT_DIR}/../lambdas/" "${SCRIPT_DIR}/../templates/lambda/code" project_name="${LAMBDA_NAME}"

# Run cookiecutter to generate the new infra files
cookiecutter --no-input --output-dir "${SCRIPT_DIR}/../infrastructure/lambdas" "${SCRIPT_DIR}/../templates/lambda/infrastructure" project_name="${LAMBDA_NAME}" project_description="${LAMBDA_DESCRIPTION}"

# Use yq to update the main .gitlab-ci.yml file
yq_args=("-i" ".build-lambdas.parallel.matrix += {\"LAMBDA\": [\"${LAMBDA_NAME}\"]}" "$SCRIPT_DIR/../.gitlab-ci.yml")
yq "${yq_args[@]}"

