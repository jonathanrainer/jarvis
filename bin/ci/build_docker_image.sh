#!/bin/bash

set -eo

LAMBDA_NAME=$1
VERSION=$2
CI_DEFAULT_BRANCH=$3
CI_COMMIT_BRANCH=$4

# Print out parameters
echo "LAMBDA_NAME=$LAMBDA_NAME"
echo "VERSION=$VERSION"
echo "CI_DEFAULT_BRANCH=$CI_DEFAULT_BRANCH"
echo "CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH"

# Install the tools we need
apk add aws-cli jq
# Grab the script directory
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR/../../lambdas/$LAMBDA_NAME"
# Grab the repo URI
REPO_URI=$(aws ecr describe-repositories --output json | jq --arg name "jarvis/$LAMBDA_NAME" -r '.repositories[] | select(.repositoryName | contains($name)).repositoryUri')
echo "REPO_URI: $REPO_URI"
# Login to ECR
aws ecr get-login-password | docker login --username AWS --password-stdin "$REPO_URI"
# Build and tag the image with all the right tags
docker build . -t "$REPO_URI:v$VERSION" -t "$REPO_URI:$VERSION"
# If you're on master then add the tag latest and remove it from previously tagged image, if one exists
if [ "$CI_DEFAULT_BRANCH" = "$CI_COMMIT_BRANCH" ]; then
  aws ecr batch-delete-image --repository-name jarvis/$LAMBDA_NAME --image-ids imageTag=latest
  docker tag "$REPO_URI:$VERSION" "$REPO_URI:latest"
fi
# Push all the tags to ECR at once
docker push -a "$REPO_URI"