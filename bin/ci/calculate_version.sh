#!/bin/bash

set -eo pipefail

CI_PROJECT_ID=$1
CI_DEFAULT_BRANCH=$2
CI_PIPELINE_ID=$3
CI_COMMIT_SHA=$4
LAMBDA_NAME=$5
MODE=$6
CI_COMMIT_BRANCH=$7

# Print out Params
echo "CI_PROJECT_ID=$CI_PROJECT_ID"
echo "CI_DEFAULT_BRANCH=$CI_DEFAULT_BRANCH"
echo "CI_PIPELINE_ID=$CI_PIPELINE_ID"
echo "CI_COMMIT_SHA=$CI_COMMIT_SHA"
echo "LAMBDA_NAME=$LAMBDA_NAME"
echo "CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH"
echo "CI_MERGE_REQUEST_ID=$CI_MERGE_REQUEST_ID"


# Grab the script directory
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# Install required dependencies
apt-get update
apt-get install -y jq
# Grab the latest tag from Git, pull out the version number
VERSION_RAW=$(curl -Ss --request GET "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/tags?search=^${LAMBDA_NAME}" | jq --arg lambda "$LAMBDA_NAME" -r '.[0] | .name // "$lambda/0.0.0"')
DEPLOYED_VERSION=$(echo "$VERSION_RAW" | cut -d "/" -f 2)
echo "Current Deployed Version is $DEPLOYED_VERSION"
# Compare that version to the one that poetry knows about
cd "${SCRIPT_DIR}/../../lambdas/${LAMBDA_NAME}" || exit
DEV_VERSION=$(poetry version --short)
echo "Version to be built is $DEV_VERSION"
# Compare the dev version to the deployed version
if [[ "$DEV_VERSION" == "$DEPLOYED_VERSION" ]]; then
  echo "Cannot build Lambda as the version to be built has already been published, so when 'main' is built this won't work"
  exit 1
fi
case $MODE in
  branch)
    echo "This build is a branch build"
    if [[ "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]]; then
      echo "This build is a non-main build"
      VERSION="$DEPLOYED_VERSION-$CI_COMMIT_BRANCH.$CI_PIPELINE_ID.$CI_COMMIT_SHA"
    # Otherwise we're building the Dev version
    else
      VERSION=$DEV_VERSION
    fi
  ;;
  mr)
    echo "This build is a merge request build"
    VERSION="$DEPLOYED_VERSION-MR.$CI_PIPELINE_ID.$CI_COMMIT_SHA"
  ;;
esac
# Write out the version into an env file
echo "Building version $VERSION"
echo "VERSION=$VERSION" >> version.env