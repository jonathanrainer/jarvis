import json


class {{ cookiecutter.project_name.capitalize() }}:

    def run(self):
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            'body': json.dumps({
                'message': "Hello World!"
            }),
            "isBase64Encoded": False
        }
