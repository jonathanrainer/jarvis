import logging

from {{ cookiecutter.project_name }} import {{ cookiecutter.project_name.capitalize() }}

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):
    return run()


def run():
    return {{ cookiecutter.project_name.capitalize() }}().run()


if __name__ == "__main__":
    print(run())
