module "{{ cookiecutter.project_name }}" {
  count = contains(var.lambdas_released, "{{ cookiecutter.project_name }}") ? 1 : 0
  source = "./modules/jarvis_lambda"

  api_id         = aws_apigatewayv2_api.jarvis.id
  additional_iam_statements         = []
  api_gateway_execution_arn         = aws_apigatewayv2_api.jarvis.execution_arn
  base_lambda_policy_arn            = aws_iam_policy.jarvis_lambda.arn
  environment_variables             = {}
  lambda_name                       = "{{ cookiecutter.project_name }}"
  lambda_description                = "{{ cookiecutter.project_description }}"
  api_routes = ["GET /{{ cookiecutter.project_name }}"]
}