import os
import pathlib

os.chdir("..")
p = pathlib.Path("{{cookiecutter.project_name}}")
f = p.joinpath("{{cookiecutter.project_name}}.tf")
f.rename("./{{cookiecutter.project_name}}.tf")
p.rmdir()