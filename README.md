# Jarvis

A repository to contain all the small automation scripts I write, along with the infrastructure to create/run them.

## Adding new Lambdas

If you want to add a new lambda then from the repo root run 

```shell
./bin/lambdagen <<LAMBDA_NAME>>
```

This will generate/update the appropriate files