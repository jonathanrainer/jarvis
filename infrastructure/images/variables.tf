variable "lambdas" {
  type = list(string)
  description = "A list of all the managed lambdas that make up Jarvis"
}