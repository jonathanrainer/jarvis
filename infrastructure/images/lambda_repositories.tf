resource "aws_ecr_repository" "lambda_repository" {
  for_each = toset(var.lambdas)
  name                 = "jarvis/${each.value}"
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_lifecycle_policy" "mr_expiration" {
  for_each   = toset(var.lambdas)
  repository = aws_ecr_repository.lambda_repository[each.value].name

  policy = jsonencode({
    rules = [
      {
        rulePriority = 1
        description  = "Expire images that have MR in them after 28 days, to clear them up"
        selection    = {
          tagStatus      = "tagged"
          tagPatternList = ["*MR*"]
          countType      = "sinceImagePushed"
          countUnit      = "days"
          countNumber    = 28
        },
        action = {
          type = "expire"
        }
      }
    ]
  })
}