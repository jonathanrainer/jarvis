variable "lambdas_released" {
  type = list(string)
  description = "A list of all the lambdas that are released"
}