locals {
  titan_bucket_name = "jarvis-calendars"
}

resource "aws_s3_bucket" "calendars_bucket" {
  bucket = local.titan_bucket_name
}

resource "aws_s3_bucket_versioning" "calendars_bucket_versioning_policy" {
  bucket = local.titan_bucket_name
  versioning_configuration {
    status = "Enabled"
  }
}

module "titan" {
  count = contains(var.lambdas_released, "titan") ? 1 : 0
  source = "./modules/jarvis_lambda"

  api_id         = aws_apigatewayv2_api.jarvis.id
  additional_iam_statements         = [
    {
      actions   = ["s3:GetObject", "s3:GetObjectAttributes", "s3:GetObjectVersion", "s3:GetObjectVersionAttributes",
      "s3:ListBucket", "s3:ListBucketVersions"]
      resources = ["${aws_s3_bucket.calendars_bucket.arn}/*"]
      effect = "Allow"
    }
  ]
  api_gateway_execution_arn         = aws_apigatewayv2_api.jarvis.execution_arn
  base_lambda_policy_arn            = aws_iam_policy.jarvis_lambda.arn
  environment_variables             = {
    "TITAN_BUCKET_NAME": aws_s3_bucket.calendars_bucket.bucket
  }
  lambda_name                       = "titan"
  lambda_description                = "This function computes a standard day and returns it in iCal format"
  api_routes = [
  "GET /titan/{year}/{month}/{day}"
]
}