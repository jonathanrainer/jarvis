module "polus" {
  count = contains(var.lambdas_released, "polus") ? 1 : 0
  source = "./modules/jarvis_lambda"

  api_id         = aws_apigatewayv2_api.jarvis.id
  additional_iam_statements         = []
  api_gateway_execution_arn         = aws_apigatewayv2_api.jarvis.execution_arn
  base_lambda_policy_arn            = aws_iam_policy.jarvis_lambda.arn
  environment_variables             = {}
  lambda_name                       = "polus"
  lambda_description                = ""
  api_routes = [
    "PUT /polus/find_occurrences",
    "GET /polus/next_occurrence",
    "GET /polus/{year}/{month}/{day}"
    ]
}

resource "aws_cloudwatch_event_rule" "daily_polus_trigger" {
  count = contains(var.lambdas_released, "polus") ? 1 : 0
  name        = "daily-polus-trigger"
  description = "Trigger Polus daily to poll for new events so that our data on PMQs can be kept up to date"

  schedule_expression = "cron(0 10 * * ? *)"
}

resource "aws_cloudwatch_event_target" "polus" {
  count = contains(var.lambdas_released, "polus") ? 1 : 0
  arn  = module.polus[0].lambda_arn
  rule = aws_cloudwatch_event_rule.daily_polus_trigger[0].name
  input = jsonencode({
    httpMethod: "PUT"
  })
}

resource "aws_lambda_permission" "allow_polus_trigger" {
  count = contains(var.lambdas_released, "polus") ? 1 : 0
  statement_id = "AllowExecutionFromEventBridge"
  action        = "lambda:InvokeFunction"
  function_name = module.polus[0].lambda_name
  principal     = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.daily_polus_trigger[0].arn
}