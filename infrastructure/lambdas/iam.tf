data "aws_region" "current" {}

data "aws_caller_identity" "identity" {}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    actions = ["logs:CreateLogGroup","logs:CreateLogStream", "logs:PutLogEvents"]
    effect = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_policy" "jarvis_lambda" {
  policy = data.aws_iam_policy_document.lambda_policy.json
}