variable "api_id" {
  type = string
  description = "The ID from the API declaration that this Lambda function should be part of"
}

variable "api_gateway_execution_arn" {
  type = string
  description = "The Execution ARN for the API defined"
}

variable "lambda_name" {
  type = string
  description = "The name of the lambda function"
}

variable "lambda_description" {
  type = string
  description = "A short description of the lambda for AWS"
}

variable "api_routes" {
  type = list(string)
  description = "The specification of the routes to the API, including path variables etc."
}

variable "base_lambda_policy_arn" {
  type = string
  description = "The basic IAM policy that is used by all lambdas, allows pulling of images etc."
}

variable "additional_iam_statements" {
  type = list(object({actions = list(string), resources = list(string), effect = string }))
}

variable "environment_variables" {
  type = map(string)
}

