data "aws_ecr_repository" "lambda_ecr_repo" {
  name = "jarvis/${var.lambda_name}"
}

data "aws_iam_policy_document" "lambda_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "lambda_image_pull_policy" {
  statement {
    actions = ["ecr:BatchGetImage", "ecr:GetDownloadUrlForLayer"]
    resources = [data.aws_ecr_repository.lambda_ecr_repo.arn]
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "extra_iam_policy" {
  dynamic "statement" {
    for_each = toset(var.additional_iam_statements)
    content {
      actions = statement.value.actions
      effect = statement.value.effect
      resources = statement.value.resources
    }
  }
}

data "aws_iam_policy_document" "empty_policy" {
  statement {
    actions = ["none:null"]
    effect = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda_extra_iam_policy" {
  policy = length(var.additional_iam_statements) > 0 ? data.aws_iam_policy_document.extra_iam_policy.json : data.aws_iam_policy_document.empty_policy.json
}

resource "aws_iam_role" "lambda_role" {
  name = var.lambda_name

  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy.json

  managed_policy_arns = [
    var.base_lambda_policy_arn,
    aws_iam_policy.lambda_extra_iam_policy.arn
  ]
}