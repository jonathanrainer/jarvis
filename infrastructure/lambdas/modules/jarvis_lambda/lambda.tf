data aws_ecr_image "lambda_image" {
  repository_name = "jarvis/${var.lambda_name}"
  image_tag = "latest"
}

locals {
  # Extract the version tag from the image that is tagged as latest
  lambda_version = [for x in data.aws_ecr_image.lambda_image.image_tags : x if x != "latest" && startswith(x, "v")][0]
}

resource "aws_lambda_function" "lambda" {
  description = var.lambda_description
  image_uri = "${data.aws_ecr_repository.lambda_ecr_repo.repository_url}:${local.lambda_version}"
  package_type = "Image"
  timeout = 30

  function_name = var.lambda_name
  role          = aws_iam_role.lambda_role.arn
  environment {
    variables = var.environment_variables
  }
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${var.api_gateway_execution_arn}/*/*"
}