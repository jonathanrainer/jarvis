resource "aws_apigatewayv2_integration" "lambda" {
  api_id = var.api_id

  integration_uri    = aws_lambda_function.lambda.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "lambda_route" {
  for_each = toset(var.api_routes)
  api_id = var.api_id

  route_key = each.value
  target    = "integrations/${aws_apigatewayv2_integration.lambda.id}"
}


