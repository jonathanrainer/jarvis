locals {
  kairos_bucket_names = "jarvis-routines"
}

resource "aws_s3_bucket" "routines_bucket" {
  bucket = local.kairos_bucket_names
}

resource "aws_s3_bucket_versioning" "routines_bucket_versioning_policy" {
  bucket = local.kairos_bucket_names
  versioning_configuration {
    status = "Enabled"
  }
}

module "kairos" {
  count = contains(var.lambdas_released, "kairos") ? 1 : 0
  source = "./modules/jarvis_lambda"

  api_id         = aws_apigatewayv2_api.jarvis.id
  additional_iam_statements         = [
    {
      actions   = ["s3:GetObject", "s3:GetObjectAttributes", "s3:GetObjectVersion", "s3:GetObjectVersionAttributes",
      "s3:ListBucket", "s3:ListBucketVersions"]
      resources = ["${aws_s3_bucket.routines_bucket.arn}/*"]
      effect = "Allow"
    }
  ]
  api_gateway_execution_arn         = aws_apigatewayv2_api.jarvis.execution_arn
  base_lambda_policy_arn            = aws_iam_policy.jarvis_lambda.arn
  environment_variables             = {
      "KAIROS_BUCKET_NAME": aws_s3_bucket.routines_bucket.bucket
    }
  lambda_name                       = "kairos"
  lambda_description = "This function computes the set of checklists and tasks required for the given day and returns them in TaskPaper format for consumption by iOS Shortcuts"
  api_routes = [
    "GET /kairos/{year}/{month}/{day}"
    ]
}