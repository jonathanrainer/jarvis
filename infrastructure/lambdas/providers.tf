terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  cloud {
    organization = "jarvis-infrastructure"

    workspaces {
      name = "lambda-infrastructure"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-2"
  default_tags {
    tags = {
      project = "jarvis"
    }
  }
}