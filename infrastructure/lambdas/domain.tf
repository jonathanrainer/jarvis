locals {
  lambda_domain = "jarvis.jonathan-rainer.com"
  root_domain = "jonathan-rainer.com"
}

resource "aws_route53_zone" "jonathan_rainer_domain" {
  name = "jonathan-rainer.com"
}

resource "aws_route53_record" "jarvis_A" {
  zone_id = aws_route53_zone.jonathan_rainer_domain.id
  name    = local.lambda_domain
  type    = "A"

  alias {
    evaluate_target_health = true
    name                   = aws_apigatewayv2_domain_name.jarvis.domain_name_configuration[0].target_domain_name
    zone_id                = aws_apigatewayv2_domain_name.jarvis.domain_name_configuration[0].hosted_zone_id
  }
}

resource "aws_apigatewayv2_domain_name" "jarvis" {
  domain_name = local.lambda_domain
  domain_name_configuration {
      certificate_arn = aws_acm_certificate_validation.jarvis_validate.certificate_arn
      endpoint_type = "REGIONAL"
      security_policy = "TLS_1_2"
  }
}

resource "aws_apigatewayv2_api_mapping" "jarvis_mapping" {
  api_id      = aws_apigatewayv2_api.jarvis.id
  domain_name = aws_apigatewayv2_domain_name.jarvis.id
  stage = aws_apigatewayv2_stage.lambda.id
}

resource "aws_route53_record" "jarvis_CNAME" {
  for_each = {
    for dvo in aws_acm_certificate.jarvis_cert.domain_validation_options : dvo.domain_name => {
      name    = dvo.resource_record_name
      record  = dvo.resource_record_value
      type    = dvo.resource_record_type
      zone_id = aws_route53_zone.jonathan_rainer_domain.id
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = each.value.zone_id
}

resource "aws_acm_certificate" "jarvis_cert" {
  domain_name       = "*.jonathan-rainer.com"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "jarvis_validate" {
  certificate_arn         = aws_acm_certificate.jarvis_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.jarvis_CNAME : record.fqdn]
}