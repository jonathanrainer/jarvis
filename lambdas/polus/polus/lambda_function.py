import logging
import datetime

from polus import Polus

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):
    logging.info("Reacting to event '%s'", event)
    logging.info("Context is '%s'", context)

    match event["httpMethod"]:
        case "GET":
            logging.info("Matched GET Request...")
            if event["pathParameters"] is None:
                logging.info("No path parameters patched, fetching next unwatched occurrence")
                logging.info(f"Finding next unwatched occurrence...")
            else:
                path_parameters = event["pathParameters"]
                date = datetime.date(int(path_parameters["year"]),
                                     int(path_parameters["month"]), int(path_parameters["day"]))
                logging.info(f"Finding occurrence on {date.strftime('%Y-%m-%d')}")
        case "PUT":
            logging.info("Matched PUT Request...")
            logging.info("Scanning for new occurrences...")
    return run()


def run():
    return Polus().run()


if __name__ == "__main__":
    print(run())
