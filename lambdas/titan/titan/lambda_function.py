import datetime
import logging
import os
from pathlib import Path

import pytz

from titan import Titan
from calendar_accessor import S3CalendarAccessor, LocalCalendarAccessor

import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):
    logging.debug("Reacting to event '%s'", event)
    logging.debug("Context is '%s'", context)

    path_params = event["pathParameters"]
    logging.info("Extracting date from '%s'", event["pathParameters"])
    date = datetime.date(int(path_params["year"]), int(path_params["month"]), int(path_params["day"]))
    logging.info("Calculating day template for %s", date.strftime("%Y-%m-%d"))

    query_string_params = event["queryStringParameters"]
    timezone = pytz.timezone("Europe/London")
    if query_string_params:
        tz = query_string_params.get("timezone", "Europe/London")
        timezone = pytz.timezone(tz)
    logging.debug("Calculating day using timezone %s", timezone.zone)

    return run(date, timezone)


def run(date: datetime.date, tz: datetime.tzinfo):
    if os.environ.get("AWS_LAMBDA_FUNCTION_NAME"):
        logging.info("Running in AWS Lambda, will pull routines from S3...")
        return Titan(S3CalendarAccessor(boto3.resource("s3"))).run(date, tz)
    else:
        logging.info("Running locally, will pull routines from filesystem...")
        return Titan(
            LocalCalendarAccessor(Path(os.path.dirname(os.path.realpath(__file__)), "..", "templates"))
        ).run(date, tz)


if __name__ == "__main__":
    print(run(datetime.date.today(), pytz.timezone("Europe/London")))
