import datetime
import json

from calendar_accessor import CalendarAccessor
from calendar_converter import CalendarConverter


class Titan:
    _calendar_accessor = None
    _calendar_converter = None

    def __init__(self, calendar_accessor: CalendarAccessor):
        self._calendar_accessor = calendar_accessor
        self._calendar_converter = CalendarConverter()

    def run(self, date: datetime.date, tz: datetime.tzinfo):
        # Grab template from templates directory
        raw_calendar = self._calendar_accessor.get_calendar()
        # Hydrate the template into some in iCal format
        cal = self._calendar_converter.convert_calendar(raw_calendar, date, tz, datetime.datetime.now(tz))
        # Push that back to the user
        return self.wrap_response_in_lambda_body({
            'ical': cal.to_ical(True).decode("utf-8")
        })

    def wrap_response_in_lambda_body(self, body):
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            'body': json.dumps(body),
            "isBase64Encoded": False
        }
