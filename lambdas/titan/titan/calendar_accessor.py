import json
import os
from abc import ABC, abstractmethod
from pathlib import Path


class CalendarAccessor(ABC):

    @abstractmethod
    def get_calendar(self):
        return


class S3CalendarAccessor(CalendarAccessor):
    s3_client = None

    def __init__(self, s3_client):
        self.s3_client = s3_client

    def get_s3_json_file(self, key) -> dict:
        content_object = self.s3_client.Object(os.environ["TITAN_BUCKET_NAME"], key)
        file_content = content_object.get()['Body'].read().decode('utf-8')
        return json.loads(file_content)

    def get_calendar(self):
        return self.get_s3_json_file("normal_day.json")


class LocalCalendarAccessor(CalendarAccessor):
    routine_directory = None

    def __init__(self, routine_directory: Path):
        self.routine_directory = routine_directory

    def get_local_routine(self, file_name: str) -> dict:
        with open(Path(self.routine_directory, file_name)) as f:
            return json.load(f)

    def get_calendar(self):
        return self.get_local_routine("normal_day.json")
