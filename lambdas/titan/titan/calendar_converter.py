from abc import ABC, abstractmethod
from datetime import date, datetime, time

from icalendar import Calendar, Event

import uuid


class UidFactory(ABC):

    @abstractmethod
    def get_uid(self):
        return ""


class PlatformUidFactory(UidFactory):

    def get_uid(self):
        return f"{uuid.uuid4().hex}@jarvis.com"


class CalendarConverter:
    _prod_id = "-//Jarvis//Titan//EN"
    _uid_factory = None

    def __init__(self, uid_factory: UidFactory = PlatformUidFactory()):
        self._uid_factory = uid_factory

    def convert_calendar(self, raw_calendar, day_of_calendar: date, timezone: datetime.tzinfo,
                         creation_time) -> Calendar:
        cal = Calendar()
        cal.add('version', '2.0')
        cal.add('prodid', self._prod_id)
        for ev in raw_calendar["day"]:
            event = Event()
            event.add('summary', ev['title'])
            event.add('dtstart',
                      datetime.combine(day_of_calendar, datetime.strptime(ev["start_time"], "%H:%M:%S").time(),
                                       tzinfo=timezone))
            event.add('dtend', datetime.combine(day_of_calendar, datetime.strptime(ev["end_time"], "%H:%M:%S").time(),
                                                tzinfo=timezone))
            event.add("dtstamp", creation_time)
            event.add("uid", self._uid_factory.get_uid())
            cal.add_component(event)
        return cal
