import datetime

import pytz

from calendar_converter import CalendarConverter, UidFactory
from icalendar import vDatetime

CREATION_TIME = datetime.datetime.now()
FAKE_UID_1 = "5908ba5b-0592-4c17-9914-2e615ef64100@jarvis.com"
FAKE_UID_2 = "470d6fde-33e2-49a3-ab59-d89d89f24e28@jarvis.com"
FAKE_UID_3 = "1602a9c3-d9f4-441a-9856-5357f811cf8d@jarvis.com"
ICAL_OUTPUT = f'''BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Jarvis//Titan//EN
BEGIN:VEVENT
SUMMARY:Me Time 💆
DTSTART;TZID=Europe/London:20231122T060000
DTEND;TZID=Europe/London:20231122T083000
DTSTAMP:{vDatetime(CREATION_TIME).to_ical().decode("utf-8")}Z
UID:{FAKE_UID_1}
END:VEVENT
BEGIN:VEVENT
SUMMARY:Lunch 🍕
DTSTART;TZID=Europe/London:20231122T120000
DTEND;TZID=Europe/London:20231122T130000
DTSTAMP:{vDatetime(CREATION_TIME).to_ical().decode("utf-8")}Z
UID:{FAKE_UID_2}
END:VEVENT
BEGIN:VEVENT
SUMMARY:Play With Oli 🐈
DTSTART;TZID=Europe/London:20231122T163000
DTEND;TZID=Europe/London:20231122T165500
DTSTAMP:{vDatetime(CREATION_TIME).to_ical().decode("utf-8")}Z
UID:{FAKE_UID_3}
END:VEVENT
END:VCALENDAR
'''.replace("\n", "\r\n")


class FakeUidFactory(UidFactory):

    counter = -1
    fake_uids = [
        FAKE_UID_1,
        FAKE_UID_2,
        FAKE_UID_3,
    ]

    def __init__(self):
        self.counter = 0

    def get_uid(self):
        result = self.fake_uids[self.counter]
        self.counter += 1
        return result



def test_convert_raw_calendar():
    raw_calendar = {
        "day": [
            {
                "title": "Me Time 💆",
                "start_time": "06:00:00",
                "end_time": "08:30:00"
            },
            {
                "title": "Lunch 🍕",
                "start_time": "12:00:00",
                "end_time": "13:00:00"
            },
            {
                "title": "Play With Oli 🐈",
                "start_time": "16:30:00",
                "end_time": "16:55:00"
            },
        ]
    }

    converter = CalendarConverter(FakeUidFactory())

    cal = converter.convert_calendar(raw_calendar, datetime.date(2023, 11, 22), pytz.timezone('Europe/London'),
                                     CREATION_TIME)

    assert (cal.to_ical(True).decode("utf-8") == ICAL_OUTPUT)
