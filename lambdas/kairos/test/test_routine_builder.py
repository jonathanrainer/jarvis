import datetime

from networkx.utils import graphs_equal

from domain import ROOT_NODE_NAME
from routine_builder import RoutineBuilder, RoutineAccessor
from networkx import DiGraph


class TestRoutineAccessor(RoutineAccessor):

    def get_day_by_day_routines(self) -> dict:
        return {
            "routines": {
                "Exercise": {
                    "tracked": False,
                    "due_time": "22:00:00",
                    "tasks": [
                        {"Running": {}},
                        {"Running": {}},
                        {"Walking": {}},
                        {"Running": {}},
                        {},
                        {"Walking": {}},
                        {}
                    ]
                }
            }
        }

    def get_every_day_routines(self) -> dict:
        return {
            "routines": {
                "Morning": {
                    "tracked": True,
                    "due_time": "12:34:56",
                    "tasks": [
                        {
                            "Wash My Face": {}
                        }
                    ]
                }
            }
        }


def test_build_routines():
    rb = RoutineBuilder(TestRoutineAccessor())
    actual = rb.build_routines()

    assert set([x.name for x in actual[0]]) == {"Morning", "Exercise"}
    assert set([x.due_time for x in actual[0]]) == {datetime.time(12, 34, 56), datetime.time(22)}
    assert set([x.tracked for x in actual[0]]) == {True, False}


def test_deserialise_task():
    rb = RoutineBuilder(TestRoutineAccessor())
    actual = rb.deserialise_task({"Wash My Face": {}})
    expected = DiGraph()
    expected.add_node(ROOT_NODE_NAME, ordering=-1)
    expected.add_node("Wash My Face", ordering=-1)
    expected.add_edge(ROOT_NODE_NAME, "Wash My Face")

    assert graphs_equal(actual, expected)


def test_deserialise_more_complex_task():
    rb = RoutineBuilder(TestRoutineAccessor())
    actual = rb.deserialise_task({"Wash My Face": [
        {"Turn On Water": {}},
        {"Apply Cleanser": {}},
        {"Wash Off Cleanser": {}}
    ]})
    expected = DiGraph()
    expected.add_node(ROOT_NODE_NAME, ordering=-1)
    expected.add_node("Wash My Face", ordering=-1)
    expected.add_node("Turn On Water", ordering=0)
    expected.add_node("Apply Cleanser", ordering=1)
    expected.add_node("Wash Off Cleanser", ordering=2)

    expected.add_edge(ROOT_NODE_NAME, "Wash My Face")
    expected.add_edge("Wash My Face", "Turn On Water")
    expected.add_edge("Wash My Face", "Apply Cleanser")
    expected.add_edge("Wash My Face", "Wash Off Cleanser")

    assert graphs_equal(actual, expected)


def test_deserialise_even_more_complex_task():
    rb = RoutineBuilder(TestRoutineAccessor())
    actual = rb.deserialise_task({"Wash My Face": [
        {"Turn On Water": [
            {
                "Create Water": {
                    "Build Hydrogen Atoms": {},
                    "Stage Nuclear Reaction": [
                        {"Apply for Nuclear Permit": {}}
                    ]}
            },
        ]},
    ]})
    expected = DiGraph()

    expected.add_node(ROOT_NODE_NAME, ordering=-1)
    expected.add_node("Wash My Face", ordering=-1)
    expected.add_node("Turn On Water", ordering=0)
    expected.add_node("Create Water", ordering=0)
    expected.add_node("Build Hydrogen Atoms", ordering=-1)
    expected.add_node("Stage Nuclear Reaction", ordering=-1)
    expected.add_node("Apply for Nuclear Permit", ordering=0)

    expected.add_edge(ROOT_NODE_NAME, "Wash My Face")
    expected.add_edge("Wash My Face", "Turn On Water")
    expected.add_edge("Turn On Water", "Create Water")
    expected.add_edge("Create Water", "Build Hydrogen Atoms")
    expected.add_edge("Create Water", "Stage Nuclear Reaction")
    expected.add_edge("Stage Nuclear Reaction", "Apply for Nuclear Permit")

    assert graphs_equal(actual, expected)
