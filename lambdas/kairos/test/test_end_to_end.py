import datetime
import json
import os
from pathlib import Path

from kairos import Kairos
from routine_builder import LocalRoutineAccessor


def test_system_end_to_end():
    test_directory = Path(os.path.dirname(os.path.realpath(__file__)), "test_files")
    k = Kairos(LocalRoutineAccessor(test_directory))

    expected_routines = json.loads(k.run(datetime.date(2023, 3, 17))["body"])["routines"]

    assert expected_routines[0] == "Morning @due(2023-03-17T09:00:00+00:00):\n\t- Check for Washing Load\n\t- Empty Bins into Kitchen Bin\n\t- Put on Washing\n\t- Plan Out Day\n\t- Wash\n\t\t- Shower\n\t\t- Wash Face\n\t\t- Do Irregular Tasks\n\t\t- Clean Teeth\n\t\t- Apply Deodorant\n\t- Get Dressed\n"
