import datetime
import json

from networkx import DiGraph

from domain import Routine, ROOT_NODE_NAME
from response_builder import ResponseBuilder


def test_response_builder():
    resp_builder = ResponseBuilder()

    morning_routine = DiGraph()
    morning_routine.add_node(ROOT_NODE_NAME, ordering=-1)
    morning_routine.add_node("Wash My Face", ordering=-1)
    morning_routine.add_node("Turn On Water", ordering=0)
    morning_routine.add_node("Apply Cleanser", ordering=1)
    morning_routine.add_node("Wash Off Cleanser", ordering=2)

    morning_routine.add_edge(ROOT_NODE_NAME, "Wash My Face")
    morning_routine.add_edge("Wash My Face", "Turn On Water")
    morning_routine.add_edge("Wash My Face", "Apply Cleanser")
    morning_routine.add_edge("Wash My Face", "Wash Off Cleanser")

    exercise_routine = DiGraph()
    exercise_routine.add_node(ROOT_NODE_NAME, ordering=-1)
    exercise_routine.add_node("Running", ordering=-1)

    exercise_routine.add_edge(ROOT_NODE_NAME, "Running")

    actual = resp_builder.create_response(
        datetime.date(2022, 10, 11), [
            Routine("Morning", morning_routine, datetime.time(12, 34, 56), True),
            Routine("Exercise", exercise_routine, datetime.time(22, 00, 00), False)
        ]
    )

    assert json.loads(actual["body"])["summary"] == "Summary - 11-10-2022\n\nExercise: Running\n"

    routines = json.loads(actual["body"])["routines"]

    assert routines[0] == "Morning @due(2022-10-11T12:34:56+01:00):\n\t- Wash My Face\n\t\t"\
                                                     "- Turn On Water\n\t\t- Apply Cleanser\n\t\t- Wash Off Cleanser\n"