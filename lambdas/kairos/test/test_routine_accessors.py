import json
import os
from pathlib import Path
from tempfile import TemporaryDirectory

import pytest

from routine_builder import LocalRoutineAccessor, S3RoutineAccessor


@pytest.mark.parametrize("routine_type,recall_function",
                         [
                             (LocalRoutineAccessor.DAY_BY_DAY_FILE, lambda temp_routines_dir: LocalRoutineAccessor(
                                 Path(temp_routines_dir)).get_day_by_day_routines()),
                             (LocalRoutineAccessor.EVERYDAY_FILE, lambda temp_routines_dir: LocalRoutineAccessor(
                                 Path(temp_routines_dir)).get_every_day_routines()),
                         ],
                         ids=["day_by_day_local", "everyday_local"])
def test_local_routine_accessor(routine_type, recall_function):
    expected_routines = {"foo": "bar"}
    with TemporaryDirectory(prefix="routines") as temp_routines_dir:
        with open(Path(temp_routines_dir, routine_type), "w") as routine_file:
            json.dump(expected_routines, routine_file)
        assert recall_function(temp_routines_dir) == expected_routines


@pytest.mark.parametrize("routine_type,recall_function",
                         [
                             (LocalRoutineAccessor.DAY_BY_DAY_FILE, lambda cache: S3RoutineAccessor(FakeS3Client(
                                 cache)).get_day_by_day_routines()),
                             (LocalRoutineAccessor.EVERYDAY_FILE, lambda cache: S3RoutineAccessor(FakeS3Client(
                                 cache)).get_every_day_routines()),
                         ],
                         ids=["day_by_day_s3", "everyday_s3"])
def test_s3_routine_accessor(routine_type, recall_function):
    os.environ["KAIROS_BUCKET_NAME"] = "bucket"
    with TemporaryDirectory(prefix="routines") as temp_routines_dir:
        with open(Path(temp_routines_dir, routine_type), "w") as day_by_day_file:
            json.dump({"foo": "bar"}, day_by_day_file)
        with open(Path(temp_routines_dir, routine_type), "rb") as day_by_day_file:
            assert recall_function({routine_type: FakeObject(day_by_day_file)}) == {"foo": "bar"}


class FakeObject:
    content = None

    def __init__(self, file):
        self.content = file

    def get(self):
        return {"Body": self.content}


class FakeS3Client:
    cache = None

    def __init__(self, cache: dict[FakeObject]):
        self.cache = cache

    def Object(self, _, key) -> FakeObject:
        return self.cache[key]
