import datetime

from networkx import DiGraph

from domain import Routine, ROOT_NODE_NAME


def test_convert_routine_to_taskpaper():
    tasks = DiGraph()
    tasks.add_node(ROOT_NODE_NAME, ordering=-1)
    tasks.add_node("Wash My Face", ordering=-1)
    tasks.add_edge(ROOT_NODE_NAME, "Wash My Face")

    routine = Routine("Morning", tasks, datetime.time(13, 0, 0))
    routine.set_due_date(datetime.date(2022, 10, 11))

    assert routine.convert_to_taskpaper() == "Morning @due(2022-10-11T13:00:00+01:00):\n\t- Wash My Face\n"


def test_convert_more_complex_routine_to_taskpaper():
    tasks = DiGraph()
    tasks.add_node(ROOT_NODE_NAME, ordering=-1)
    tasks.add_node("Wash My Face", ordering=-1)
    tasks.add_node("Turn On Water", ordering=0)
    tasks.add_node("Apply Cleanser", ordering=1)
    tasks.add_node("Wash Off Cleanser", ordering=2)

    tasks.add_edge(ROOT_NODE_NAME, "Wash My Face")
    tasks.add_edge("Wash My Face", "Wash Off Cleanser")
    tasks.add_edge("Wash My Face", "Turn On Water")
    tasks.add_edge("Wash My Face", "Apply Cleanser")

    routine = Routine("Morning", tasks, datetime.time(17, 30, 0))
    routine.set_due_date(datetime.date(2022, 10, 11))

    assert routine.convert_to_taskpaper() == \
           "Morning @due(2022-10-11T17:30:00+01:00):\n\t- Wash My Face\n\t\t- Turn On Water\n\t\t" \
           "- Apply Cleanser\n\t\t- Wash Off Cleanser\n"
