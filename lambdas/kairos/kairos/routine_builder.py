from __future__ import annotations

import datetime
import json
import os
from abc import ABC, abstractmethod
from pathlib import Path

from networkx import DiGraph

from domain import Routine, ROOT_NODE_NAME

DAYS_IN_WEEK = 7


class RoutineAccessor(ABC):
    DAY_BY_DAY_FILE = "day_by_day.json"
    EVERYDAY_FILE = "every_day.json"

    @abstractmethod
    def get_day_by_day_routines(self) -> dict:
        return {}

    @abstractmethod
    def get_every_day_routines(self) -> dict:
        return {}


class S3RoutineAccessor(RoutineAccessor):
    s3_client = None

    def __init__(self, s3_client):
        self.s3_client = s3_client

    def get_s3_json_file(self, key) -> dict:
        content_object = self.s3_client.Object(os.environ["KAIROS_BUCKET_NAME"], key)
        file_content = content_object.get()['Body'].read().decode('utf-8')
        return json.loads(file_content)

    def get_day_by_day_routines(self) -> dict:
        return self.get_s3_json_file(self.DAY_BY_DAY_FILE)

    def get_every_day_routines(self) -> dict:
        return self.get_s3_json_file(self.EVERYDAY_FILE)


class LocalRoutineAccessor(RoutineAccessor):

    routine_directory = None

    def __init__(self, routine_directory: Path):
        self.routine_directory = routine_directory

    def get_local_routine(self, file_name: str) -> dict:
        with open(Path(self.routine_directory, file_name)) as f:
            return json.load(f)

    def get_day_by_day_routines(self) -> dict:
        return self.get_local_routine(self.DAY_BY_DAY_FILE)

    def get_every_day_routines(self) -> dict:
        return self.get_local_routine(self.EVERYDAY_FILE)


class RoutineBuilder:
    routine_accessor = None

    def __init__(self, routine_accessor: RoutineAccessor):
        self.routine_accessor = routine_accessor

    def build_routines(self) -> list[list[Routine]]:
        result = []
        # First build up the everyday routines to give result the right shape
        everyday_routines = self.routine_accessor.get_every_day_routines()["routines"]
        for i in range(DAYS_IN_WEEK):
            result.append([])
            for name, routine in everyday_routines.items():
                due_time = datetime.datetime.strptime(routine["due_time"], "%H:%M:%S")
                result[i].append(Routine(
                    name,
                    self.deserialise_task(routine["tasks"]),
                    datetime.time(due_time.hour, due_time.minute, due_time.second),
                    routine["tracked"]
                ))

        day_by_day_routines = self.routine_accessor.get_day_by_day_routines()["routines"]
        # Deal with the day by day routines, loop over the routines that come out and
        # then add them to the appropriate days.
        for name, elements in day_by_day_routines.items():
            for day in range(len(result)):
                if elements["tasks"][day]:
                    due_time = datetime.datetime.strptime(elements["due_time"], "%H:%M:%S")
                    result[day].append(
                        Routine(
                            name,
                            self.deserialise_task(elements["tasks"][day]),
                            datetime.time(due_time.hour, due_time.minute, due_time.second),
                            elements["tracked"]
                        ))
        return result

    def deserialise_task(self, task: list | dict) -> DiGraph:
        graph = DiGraph()
        graph.add_node(ROOT_NODE_NAME, ordering=-1)
        return self.add_task_to_graph(graph, task, -1, ROOT_NODE_NAME)

    def add_task_to_graph(self, g: DiGraph, task: list | dict, ordering: int, parent: str = None) -> DiGraph:
        # If our task is a dictionary it's an unordered set of things to be done
        if type(task) == dict:
            # So break it up into those parts
            for name, subtasks in task.items():
                # Add the task this object represents to the task graph
                g.add_node(name, ordering=ordering)
                g.add_edge(parent, name)
                # If it has no subtasks then we've reached a leaf so move on
                if len(subtasks) == 0:
                    continue
                # Otherwise recursively call this function
                else:
                    return self.add_task_to_graph(g, subtasks, -1, name)
        # If the task that gets passed in is a list then recursively call this function, but passing in a different
        # ordering each time
        else:
            for index, subtask in enumerate(task):
                g = self.add_task_to_graph(g, subtask, index, parent)
        return g
