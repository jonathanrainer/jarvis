import logging
from datetime import date

from response_builder import ResponseBuilder
from routine_builder import RoutineBuilder


class Kairos:
    routine = None
    response_builder = None

    def __init__(self, routine_accessor):
        logging.info("Building routines...")
        self.routines = RoutineBuilder(routine_accessor).build_routines()
        self.response_builder = ResponseBuilder()

    def run(self, required_date: date):
        logging.info("Selecting routine for %s...", required_date.strftime('%Y-%m-%d'))
        selected_routines = self.routines[required_date.weekday()]
        logging.info("Creating response...")
        response = self.response_builder.create_response(required_date, selected_routines)
        return response
