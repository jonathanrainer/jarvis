from __future__ import annotations

import datetime
from zoneinfo import ZoneInfo

import networkx as nx
from networkx import DiGraph, get_node_attributes, shortest_path_length, nodes, dfs_preorder_nodes

DEFAULT_HOUR = 17

ROOT_NODE_NAME = "ROOT"


class Routine:
    name = ""
    tracked = False
    tasks = None
    due_time = None
    due_date = None

    def __init__(self, name: str, tasks: DiGraph, due_time: datetime.time, tracked: bool = False):
        self.name = name
        self.tasks = tasks
        self.tracked = tracked
        self.due_time = due_time

    def __eq__(self, other: Routine):
        return self.name == other.name and self.tasks == other.tasks

    def __str__(self):
        return f"{self.name}"

    def set_due_date(self, date: datetime.date):
        if self.due_time:
            self.due_date = datetime.datetime(date.year, date.month, date.day, self.due_time.hour, self.due_time.minute,
                                              self.due_time.second, self.due_time.microsecond,
                                              tzinfo=ZoneInfo("Europe/London"))
        else:
            self.due_date = datetime.datetime(date.year, date.month, date.day, DEFAULT_HOUR, 0, 0, 0,
                                              tzinfo=ZoneInfo("Europe/London"))

    def convert_to_taskpaper(self) -> str:
        output = f"{self.name} @due({self.due_date.isoformat()}):\n"
        enriched_nodes = []
        self.dfs_sorted_children([], self.tasks, ROOT_NODE_NAME, enriched_nodes)
        for node in enriched_nodes:
            output += "\t" * node[1] + f"- {node[0]}\n"
        return output

    def dfs_sorted_children(self, visited: [str], tasks: DiGraph, node: str, results: [str]):
        if node not in visited:
            if node != ROOT_NODE_NAME:
                results.append((node, shortest_path_length(self.tasks, ROOT_NODE_NAME, node)))
            visited.append(node)
            for neighbour in sorted(tasks[node], key=lambda node: get_node_attributes(tasks, "ordering")[node]):
                self.dfs_sorted_children(visited, self.tasks, neighbour, results)
