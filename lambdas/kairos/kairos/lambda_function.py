import datetime
import json
import logging
import os
from pathlib import Path

import boto3

from kairos import Kairos
from routine_builder import S3RoutineAccessor, LocalRoutineAccessor

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):
    logging.info("Reacting to event '%s'", event)
    logging.info("Context is '%s'", context)

    path_params = event["pathParameters"]
    logging.info("Extracting date from '%s'", event["pathParameters"])
    date = datetime.date(int(path_params["year"]), int(path_params["month"]), int(path_params["day"]))
    logging.info("Calculating routines for %s", date.strftime("%Y-%m-%d"))

    return run(date)


def run(date):
    if os.environ.get("AWS_LAMBDA_FUNCTION_NAME"):
        logging.info("Running in AWS Lambda, will pull routines from S3...")
        return Kairos(S3RoutineAccessor(boto3.resource("s3"))).run(date)
    else:
        logging.info("Running locally, will pull routines from filesystem...")
        return Kairos(
            LocalRoutineAccessor(Path(os.path.dirname(os.path.realpath(__file__)), "..", "routines"))
        ).run(date)


if __name__ == "__main__":
    print(json.dumps(run(datetime.date.today() + datetime.timedelta(days=-1)), indent=4))
