import datetime
import json

from domain import Routine, ROOT_NODE_NAME


class ResponseBuilder:

    def create_response(self, day: datetime.date, routines: list[Routine]):
        summary = "Summary - " + day.strftime("%d-%m-%Y") + "\n\n"
        taskpaper_routines = []
        for routine in routines:
            if not routine.tracked:
                joined_tasks = ",".join([t for t in routine.tasks if t != ROOT_NODE_NAME])
                summary += f"{routine.name}: {joined_tasks}\n"
            else:
                routine.set_due_date(day)
                taskpaper_routines.append(routine.convert_to_taskpaper())

        return self.wrap_response_in_expected_format(taskpaper_routines, summary)

    @staticmethod
    def wrap_response_in_expected_format(routines: list, summary: str):
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            'body': json.dumps({
                'routines': routines,
                'summary': summary
            }),
            "isBase64Encoded": False
        }
